# JMS Consumer & OSDBK

The JMS Consumer is part of OSDBK (Open Source Dienst Beveiligd Koppelvlak). OSDBK consists of four applications:  

- EbMS Core / EbMS Admin
- JMS Producer  
- JMS Consumer  
- Apache ActiveMQ  

The JMS Consumer is responsible for dispatching messages from ActiveMQ queues to EbMS Core for the outbound EbMS flow.  

On startup, it pulls the CPA's from the EbMS Core database via the SOAP CPA Service of EbMS Core and stores them in memory. When updating, adding or removing a CPA from the EbMS Core database, the JMS Consumer needs to be restarted to get current CPA's in memory.  

On receiving a message from ActiveMQ, the JMS Consumer matches the content of some JMS headers (see below) of the message with the corresponding CPA. It then proceeds to send the message via the SOAP Message Service of EbMS Core, which in turn will send the message to its destination.  

### ActiveMQ Configuration  
The JMS Consumer implements the Spring Boot preconfigured ActiveMQ Listener with XA-transactions enabled. URL and credentials can be configured in the application.yml.  

~~~
spring:
  activemq:
    broker-url: tcp://activemq:61616
    user: jmsconsumer
    password: jmsconsumer
~~~

### EbMS Core SOAP Service Configuration  
EbMS Core exposes its SOAP Services by default on port 8888. If needed, the endpoints can be configured differently:  
~~~
ebms:  
    message-service:  
        url: "http://ebms-core:8888/service/ebms"  
    cpa-service:  
        url: "http://ebms-core:8888/service/cpa"  
~~~


### Queue Listener Configuration  
The JMS Consumer implements a JMS Message Listener that can be configured to listen to an X amount of queues:  
~~~
jms-consumer:  
  listeners:  
    - queueName: "AFN_11111111111111111111_InQueue"  
      concurrency: "2-10"  
    - queueName: "AFN_22222222222222222222_InQueue"  
      concurrency: "3"  
~~~
The concurrency determines the amount of listeners that will be used for a specific queue  


### JMS Header Specification
A JMS message that is sent to the queue that is listened to by the JMS Consumer, MUST have the JMS headers set as are mentioned in the table below. The JMS Consumer uses the x_aux_action, x_aux_activity, x_aux_process_type, x_aux_receiver_id and x_aux_sender_id to find a matching CPA. The x_aux_system_msg_id is used as the messageId for the outgoing message.

| Name | Description | Max Length | Content |
|---|---|---|--|
| x_aux _action |	The CPA action |	100 |	verstrekkingAanAfnemer  |
| x_aux_activity |  The CPA service |    100 |    dgl:ontvangen:1.0  |
|x_aux_process_type|The CPA service|100|dgl:ontvangen:1.0|
|x_aux_process_version|The CPA version|100|1.0|
|x_aux_production|Mandatory Production Flag|100|Production|
|x_aux_protocol|The communication protocol|100|ebMS|
|x_aux_protocol_version|The communication protocol version|100|2.0|
|x_aux_receiver_id| The OIN of the receiving CPA Party|24|12345678901234567890 (example)|
|x_aux_sender_id|The OIN of the sending CPA Party|24|00000004003214345001 (example)|
|x_aux_system_msg_id|UUID of the message|255|UUID|
|x_aux_process_instance_id|UUID of the conversation|255|UUID|
|x_aux_seq_number|The sequence number used for message ordering feature. Default 0|255|0|
|x_aux_msg_order|Indicates if parties support message ordering. Default false|100|false|

### How to run this application locally
An example docker-compose has been provided. This can be run to test this application.
However the following variables need to be populated by means of a `.env` file:
~~~
${OSDBK_IMAGE_REPOSITORY} -- location of your Docker repository where you are housing your built OSDBK docker images
${ACTIVEMQ_IMAGE_TAG} -- the tag of your built ActiveMQ docker image
${ACTIVEMQ_POSTGRESQL_USERNAME} -- the username used to connect to the ActiveMQ database
${ACTIVEMQ_POSTGRESQL_PASSWORD} -- the password used to connect to the ActiveMQ database
${DGL_STUB_IMAGE_TAG}  -- the tag of your built stub application docker image, to simulate interactions with an external party
${DGL_STUB_MESSAGE_SENDER_OIN_AFNEMER} -- the OIN of your stubbed application
${EBMS_CORE_IMAGE_TAG} -- the tag of your built ebms-core docker image
${EBMS_JDBC_USERNAME} -- the username used to connect to the EBMS database
${EBMS_JDBC_PASSWORD} -- the password used to connect to the EBMS database
${EBMS_JMS_BROKER_USERNAME} -- the username used to connect the ebms application to the ActiveMQ instance
${EBMS_JMS_BROKER_PASSWORD} -- the password used to connect the ebms application to the ActiveMQ instance
${EBMS_POSTGRESQL_IMAGE_TAG} -- the tag of your built EBMS PostgresQL docker image
${EBMS_STUB_IMAGE_TAG} -- the tag of your built stub application docker image, to simulate interactions with an external party
${JMS_CONSUMER_ACTIVEMQ_USER} -- the username used to connect the jms-consumer application to the ActiveMQ instance
${JMS_CONSUMER_ACTIVEMQ_PASSWORD} -- the password used to connect the jms-consumer application to the ActiveMQ instance
${JMS_PRODUCER_IMAGE_TAG} -- the tag of your built jms-producer docker image
${JMS_PRODUCER_ACTIVEMQ_USER} -- the username used to connect the jms-producer application to the ActiveMQ instance
${JMS_PRODUCER_ACTIVEMQ_PASSWORD} -- the password used to connect the jms-producer application to the ActiveMQ instance
~~~

Example command to bring up the application:
~~~
docker-compose --env-file path/to/env/file/.env up
~~~