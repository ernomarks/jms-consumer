package nl.logius.osdbk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;

@SpringBootApplication
@EnableRetry
public class JmsConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(JmsConsumerApplication.class, args);
	}

}
