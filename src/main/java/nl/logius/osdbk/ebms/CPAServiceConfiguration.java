package nl.logius.osdbk.ebms;

import nl.clockwork.cpa.CPAService;
import nl.clockwork.cpa.CPAService_Service;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.BindingProvider;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

@Configuration
public class CPAServiceConfiguration {
    private static final String WSDL_LOCATION = "/wsdl/cpa_2.18.wsdl";

    @Value("${ebms.cpa-service.url}")
    private String clientEndpoint;

    @Bean
    public CPAService cpaService() throws MalformedURLException {
        URL url = getClass().getResource(WSDL_LOCATION);

        CPAService_Service service = new CPAService_Service(url);
        CPAService proxy = service.getCPAPort();

        url = new URL(clientEndpoint); // validate if it's a valid endpoint

        Map<String, Object> requestContext = ((BindingProvider) proxy).getRequestContext();
        requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url.toString());

        return proxy;
    }
}
