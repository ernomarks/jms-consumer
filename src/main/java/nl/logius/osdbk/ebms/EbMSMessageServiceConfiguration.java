package nl.logius.osdbk.ebms;

import nl.clockwork.ebms.EbMSMessageService;
import nl.clockwork.ebms.EbMSMessageService_Service;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.BindingProvider;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

@Configuration
public class EbMSMessageServiceConfiguration {
    private static final String WSDL_LOCATION = "/wsdl/ebms_2.18.wsdl";

    @Value("${ebms.message-service.url}")
    private String clientEndpoint;

    @Bean
    public EbMSMessageService messageService() throws MalformedURLException {
        URL url = getClass().getResource(WSDL_LOCATION);

        EbMSMessageService_Service service = new EbMSMessageService_Service(url);
        EbMSMessageService proxy = service.getEbMSMessagePort();

        url = new URL(clientEndpoint); // validate if it's a valid endpoint

        Map<String, Object> requestContext = ((BindingProvider) proxy).getRequestContext();
        requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, url.toString());

        return proxy;
    }

}
