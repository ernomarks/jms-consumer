package nl.logius.osdbk.util;

import nl.clockwork.ebms.MessageRequestProperties;

import java.util.HashMap;
import java.util.Map;

public class LoggingUtils {

    private LoggingUtils(){}

    public static Map<String, String> getPropertyMap(MessageRequestProperties requestProperties, Map<String, String> additionalProperties) {
        Map<String, String> properties = new HashMap<>();
        properties.put("messageId", requestProperties.getMessageId());
        properties.put("conversationId", requestProperties.getConversationId());
        properties.put("sender", EbMSBrokerUtils.stripUrnPrefixes(requestProperties.getFromPartyId()));
        properties.put("senderName", requestProperties.getFromRole());
        properties.put("receiver", EbMSBrokerUtils.stripUrnPrefixes(requestProperties.getToPartyId()));
        properties.put("receiverName", requestProperties.getToRole());
        properties.put("action", requestProperties.getAction());

        properties.putAll(additionalProperties);

        return properties;
    }

    public static Map<String, String> getPropertyMap(MessageRequestProperties requestProperties) {
        return getPropertyMap(requestProperties, new HashMap<>(4, 1.0f));
    }

}
