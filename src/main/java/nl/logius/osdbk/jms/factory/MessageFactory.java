package nl.logius.osdbk.jms.factory;

import nl.clockwork.ebms.DataSource;
import nl.clockwork.ebms.MessageRequest;
import nl.clockwork.ebms.MessageRequestProperties;
import nl.logius.osdbk.jms.cpa.CPASelectionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.oasis.open.ebxml.cpa.CollaborationProtocolAgreement;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

import nl.clockwork.ebms.Party;
import org.oasis.open.ebxml.cpa.PartyInfo;

@Component
public class MessageFactory {
    private Logger logger = LoggerFactory.getLogger(MessageFactory.class);

    public MessageRequest createMessage(TextMessage message, CollaborationProtocolAgreement cpa, String service) throws JMSException {
        logger.info("JMSConsumer:MessageFactory:createEbMSMessageContent using xml");
        MessageRequestProperties requestProperties = createMessageRequestProperties(message, cpa, service);

        DataSource datasource = new DataSource();
        datasource.setContent(message.getText().getBytes());
        datasource.setContentType("text/xml; charset=UTF-8");
        MessageRequest messageRequest = new MessageRequest();
        messageRequest.setProperties(requestProperties);
        messageRequest.getDataSource().add(datasource);

        return messageRequest;
    }


    private MessageRequestProperties createMessageRequestProperties(Message message, CollaborationProtocolAgreement cpa, String service) throws JMSException {
        String senderId = message.getStringProperty("x_aux_sender_id");
        String receiverId = message.getStringProperty("x_aux_receiver_id");
        Party fromParty = createPartyFromCPA(cpa, senderId);
        Party toParty = createPartyFromCPA(cpa, receiverId);

        MessageRequestProperties messageRequestProperties = new MessageRequestProperties();
        messageRequestProperties.setCpaId(cpa.getCpaid());
        messageRequestProperties.setMessageId(message.getStringProperty("x_aux_system_msg_id"));
        messageRequestProperties.setConversationId(message.getStringProperty("x_aux_process_instance_id"));
        messageRequestProperties.setAction(message.getStringProperty("x_aux_action"));
        messageRequestProperties.setService("urn:osb:services:" + service);
        messageRequestProperties.setFromPartyId(fromParty.getPartyId());
        messageRequestProperties.setFromRole(fromParty.getRole());
        messageRequestProperties.setToPartyId(toParty.getPartyId());
        messageRequestProperties.setToRole(toParty.getRole());

        return messageRequestProperties;
    }

    private Party createPartyFromCPA(CollaborationProtocolAgreement cpa, String partyId) {
        for (PartyInfo p : cpa.getPartyInfo()) {
            String partyIdFromCPA = p.getPartyId().get(0).getValue();
            String roleName = p.getCollaborationRole().get(0).getRole().getName();
            if (partyId.equals(partyIdFromCPA)) {
                Party party = new Party();
                party.setPartyId("urn:osb:oin:" + partyId);
                party.setRole(roleName);
                return party;
            }
        }
        throw new CPASelectionException("Error: No matching partyId found in CPA");
    }
}
