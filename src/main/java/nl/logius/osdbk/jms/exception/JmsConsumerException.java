package nl.logius.osdbk.jms.exception;

public class JmsConsumerException extends RuntimeException {
    public JmsConsumerException(Throwable t) {
        super(t);
    }
}
