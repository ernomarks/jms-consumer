package nl.logius.osdbk.jms.configuration;

import nl.logius.osdbk.jms.JmsMessageConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.JmsListenerConfigurer;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerEndpointRegistrar;
import org.springframework.jms.config.SimpleJmsListenerEndpoint;

import javax.jms.ConnectionFactory;

@Configuration
@ConditionalOnProperty(value="jms-consumer.enabled", havingValue = "true", matchIfMissing = true)
public class JmsListenerConfiguration implements JmsListenerConfigurer {

    @Autowired
    private JmsConsumerConfiguration configuration;

    @Autowired
    private JmsMessageConsumer consumer;

    @Override
    public void configureJmsListeners(JmsListenerEndpointRegistrar registrar) {
        configuration.getListeners().stream().forEach(listener -> {
            SimpleJmsListenerEndpoint endpoint = new SimpleJmsListenerEndpoint();
            endpoint.setId(listener.getQueueName());
            endpoint.setDestination(listener.getQueueName());
            endpoint.setConcurrency(listener.getConcurrency());
            endpoint.setMessageListener(consumer);
            registrar.registerEndpoint(endpoint);
        });
    }

    @Bean
    public DefaultJmsListenerContainerFactory jmsListenerContainerFactory(@Qualifier("jmsConnectionFactory") ConnectionFactory connectionFactory, JmsListenerErrorHandler errorHandler) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setErrorHandler(errorHandler);
        factory.setSessionTransacted(true);
        return factory;
    }

}
