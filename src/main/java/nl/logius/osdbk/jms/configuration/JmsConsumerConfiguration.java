package nl.logius.osdbk.jms.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "jms-consumer.configuration")
public class JmsConsumerConfiguration {
    private List<Listener> listeners;

    public static class Listener {
        private String queueName;
        private String concurrency;

        public String getQueueName() {
            return queueName;
        }

        public void setQueueName(String queueName) {
            this.queueName = queueName;
        }

        public String getConcurrency() {
            return concurrency;
        }

        public void setConcurrency(String concurrency) {
            this.concurrency = concurrency;
        }
    }

    public List<Listener> getListeners() {
        return listeners;
    }

    public void setListeners(List<Listener> listeners) {
        this.listeners = listeners;
    }
}
