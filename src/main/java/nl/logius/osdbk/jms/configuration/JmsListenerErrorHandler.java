package nl.logius.osdbk.jms.configuration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.ErrorHandler;

@Service
public class JmsListenerErrorHandler implements ErrorHandler {
    private static Log logger = LogFactory.getLog(JmsListenerErrorHandler.class);

    @Override
    public void handleError(Throwable t) {
        logger.error("Error in JMSMessageListener.", t);
    }

}