package nl.logius.osdbk.jms.validation;

enum JMSHeader {
    X_AUX_ACTION,
    X_AUX_ACTIVITY,
    X_AUX_PROCESS_TYPE,
    X_AUX_PROCESS_VERSION,
    X_AUX_PRODUCTION,
    X_AUX_PROTOCOL,
    X_AUX_PROTOCOL_VERSION,
    X_AUX_RECEIVER_ID(JMSHeader.DEFAULT_OIN_LENGTH),
    X_AUX_SENDER_ID(JMSHeader.DEFAULT_OIN_LENGTH),
    X_AUX_SYSTEM_MSG_ID(JMSHeader.DEFAULT_LONG_MAX_HEADER_LENGTH),
    X_AUX_PROCESS_INSTANCE_ID(JMSHeader.DEFAULT_LONG_MAX_HEADER_LENGTH),
    X_AUX_SEQ_NUMBER(JMSHeader.DEFAULT_LONG_MAX_HEADER_LENGTH),
    X_AUX_MSG_ORDER;

    public static final int DEFAULT_MAX_HEADER_LENGTH = 100;
    private static final int DEFAULT_LONG_MAX_HEADER_LENGTH = 255;
    private static final int DEFAULT_OIN_LENGTH = 24;
    private int maxLength;

    JMSHeader() {
        this(DEFAULT_MAX_HEADER_LENGTH);
    }

    JMSHeader(int maxLength) {
        this.maxLength = maxLength;
    }

    public int getMaxLength() {
        return maxLength;
    }
}
