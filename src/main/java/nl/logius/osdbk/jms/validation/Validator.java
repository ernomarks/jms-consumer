package nl.logius.osdbk.jms.validation;

import org.apache.commons.lang3.StringUtils;

class Validator {

    private final String name;
    private final String value;
    private final int maxLength;
    private String failureCause;
    private boolean valid = true;

    /**
     * The constructor
     *
     * @param name      The name of the JMS-header (must correspond with JMSHeaderEnum.class headers)
     * @param value     The value of the header, taken from the incoming message
     * @param maxLength The max length of the particular header. Defined in the enum
     */
    public Validator(String name, String value, int maxLength) {
        this.name = name;
        this.value = value;
        this.maxLength = maxLength;
        this.failureCause = "none";
        validate();
    }

    /**
     * The method that checks the max length and if the header is not empty
     */
    private void validate() {
        if (StringUtils.isBlank(value)) {
            failureCause = "Header value is empty";
            valid = false;
        } else if (value.length() > maxLength && maxLength > 0) {
            failureCause = "Header value has exceeded the maximum length";
            valid = false;
        }
    }

    /**
     * @return The boolean value that is set to false if a header is invalid.
     */
    public boolean isValid() {
        return valid;
    }

    /**
     * @see Object#toString()
     */
    @Override
    public String toString() {
        return "HeaderValidator{" + "name=" + name + ", value=" + value + ", maxLength=" + maxLength + ", failureCause="
                + failureCause + '}';
    }

}
