package nl.logius.osdbk.jms.cpa;

import nl.clockwork.cpa.CPAService;
import nl.clockwork.cpa.CPAServiceException_Exception;
import nl.logius.osdbk.jms.cpa.jaxb.CPAParser;
import org.oasis.open.ebxml.cpa.CollaborationProtocolAgreement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class CachedCPAService {
    private Logger logger = LoggerFactory.getLogger(CachedCPAService.class);

    @Autowired
    private CPAService cpaService;

    private List<CollaborationProtocolAgreement> cpas;

    @PostConstruct
    public void loadCPAs() throws CPAServiceException_Exception {
        cpas = cpaService.getCPAIds().stream()
                .map(id -> {
                    try {
                        String cpa = cpaService.getCPA(id);
                        return CPAParser.parse(cpa);
                    } catch (JAXBException e) {
                        logger.warn("Invalid CPA. ", e);
                        return null;
                    } catch (CPAServiceException_Exception e) {
                        logger.error(e.getMessage());
                        return null;
                    }
                }).filter(Objects::nonNull)
                .collect(Collectors.toList());

        logger.info(String.format("Loaded CPAs: %s ",
                cpas.stream().map(CollaborationProtocolAgreement::getCpaid).collect(Collectors.toList())));
    }

    public List<CollaborationProtocolAgreement> getCPAs() {
        return cpas;
    }

}
