package nl.logius.osdbk.jms.cpa;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.oasis.open.ebxml.cpa.CollaborationProtocolAgreement;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import org.oasis.open.ebxml.cpa.CollaborationRole;
import org.oasis.open.ebxml.cpa.PartyInfo;

@Component
public class CPALookupService {
    private Logger logger = LoggerFactory.getLogger(CPALookupService.class);

    @Autowired
    private CachedCPAService cpaService;

    @Cacheable("CPA")
    public CollaborationProtocolAgreement findCPA(CPASearchCriteria searchCriteria) {
        List<CollaborationProtocolAgreement> cpas = cpaService.getCPAs().stream()
                .filter(cpa -> existsPartyId(cpa, searchCriteria.getSender()) && existsPartyId(cpa, searchCriteria.getReceiver()))
                .filter(cpa -> isValidParty(cpa, searchCriteria.getSender(), searchCriteria))
                .filter(cpa -> isValidParty(cpa, searchCriteria.getReceiver(), searchCriteria))
                .collect(Collectors.toList());

        return handleSearchResult(cpas, searchCriteria);
    }

    private boolean existsPartyId(CollaborationProtocolAgreement cpa, String partyId) {
        return cpa.getPartyInfo().stream()
                        .flatMap(p -> p.getPartyId().stream())
                        .anyMatch(id -> partyId.equals(id.getValue()));
    }

    public String getService(CollaborationProtocolAgreement cpa, CPASearchCriteria searchCriteria) {
        for (PartyInfo p : cpa.getPartyInfo()) {
            for (CollaborationRole cr : p.getCollaborationRole()) {
                String serviceFromCPA = cr.getServiceBinding().getService().getValue();
                if (serviceFromCPA.equals(searchCriteria.getService()) || serviceFromCPA.equals(searchCriteria.getService() + searchCriteria.getServiceExtension())) {
                    return serviceFromCPA;
                }
            }
        }
        throw new CPASelectionException("Error: No matching service found in CPA");
    }

    private CollaborationProtocolAgreement handleSearchResult(List<CollaborationProtocolAgreement> cpas, CPASearchCriteria searchCriteria) {

        if (cpas.isEmpty()) {
            throw new CPASelectionException("Error. No CPA found for sender=" + searchCriteria.getSender() + ", receiver="
                    + searchCriteria.getReceiver() + ", action=" + searchCriteria.getAction() + " and service=" + searchCriteria.getService());
        } else if (cpas.size() == 1) {
            logger.debug(String.format("Valid CPA found for sender %s and receiver %s : %s ",
                    searchCriteria.getSender(), searchCriteria.getReceiver(), cpas.get(0).getCpaid()));
            return cpas.get(0);
        } else {
            StringBuilder foundCPAIds = new StringBuilder("");
            for (CollaborationProtocolAgreement cpa : cpas) {
                foundCPAIds.append(cpa.getCpaid()).append(", ");
            }

            throw new CPASelectionException("Error. Multiple CPA's found for message with sender="
                    + searchCriteria.getSender() + ", receiver=" + searchCriteria.getReceiver() + ", action=" + searchCriteria.getAction()
                    + " and service=" + searchCriteria.getService() + ". CPAs: " + foundCPAIds.toString());
        }
    }

    private boolean isValidParty(CollaborationProtocolAgreement cpa, String partyId, CPASearchCriteria searchCriteria) {
        for (PartyInfo p : cpa.getPartyInfo()) {
            String cpaPartyId = p.getPartyId().get(0).getValue();

            if (StringUtils.equals(cpaPartyId, partyId) && checkServiceAndAction(p, searchCriteria)) {
                return true;
            }
        }
        return false;
    }

    private boolean checkServiceAndAction(PartyInfo partyInfo, CPASearchCriteria searchCriteria) {
        AtomicBoolean isValid = new AtomicBoolean(false);

        partyInfo.getCollaborationRole().forEach(cr -> {
            if (StringUtils.equals(cr.getServiceBinding().getService().getValue(), searchCriteria.getService())
                    || StringUtils.equals(cr.getServiceBinding().getService().getValue(), searchCriteria.getService() + searchCriteria.getServiceExtension())) {
                cr.getServiceBinding().getCanReceive().forEach(canReceive -> {
                    if (StringUtils.equals(canReceive.getThisPartyActionBinding().getAction(), searchCriteria.getAction())) {
                        isValid.set(true);
                    }
                });
                cr.getServiceBinding().getCanSend().forEach(canSend -> {
                    if (StringUtils.equals(canSend.getThisPartyActionBinding().getAction(), searchCriteria.getAction())) {
                        isValid.set(true);
                    }
                });
            }
        });
        return isValid.get();
    }

}
