package nl.logius.osdbk.jms.cpa.jaxb;

import org.oasis.open.ebxml.cpa.CollaborationProtocolAgreement;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

public class CPAParser {

    private CPAParser(){}

    public static CollaborationProtocolAgreement parse(String xml) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(CollaborationProtocolAgreement.class);
        StringReader reader = new StringReader(xml);

        Unmarshaller unmarshaller = jc.createUnmarshaller();
        return (CollaborationProtocolAgreement) unmarshaller.unmarshal(reader);
    }

}
