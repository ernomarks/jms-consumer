package nl.logius.osdbk.jms.cpa;

public class CPASelectionException extends RuntimeException {

    public CPASelectionException(String message) {
        super(message);
    }

    public CPASelectionException(Exception e) {
        super(e);
    }

}
