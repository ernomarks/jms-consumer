package nl.logius.osdbk.jms;

import com.sun.xml.ws.client.ClientTransportException;
import nl.clockwork.ebms.EbMSMessageService;
import nl.clockwork.ebms.EbMSMessageServiceException_Exception;
import nl.clockwork.ebms.MessageRequest;
import nl.logius.osdbk.jms.cpa.CPALookupService;
import nl.logius.osdbk.jms.cpa.CPASearchCriteria;
import nl.logius.osdbk.jms.exception.JmsConsumerException;
import nl.logius.osdbk.jms.factory.MessageFactory;
import nl.logius.osdbk.jms.validation.JMSHeaderValidationException;
import nl.logius.osdbk.jms.validation.JMSHeaderValidator;
import nl.logius.osdbk.util.LoggingUtils;
import org.oasis.open.ebxml.cpa.CollaborationProtocolAgreement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Component
public class JmsMessageConsumer implements MessageListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(JmsMessageConsumer.class);

    @Autowired
    private EbMSMessageService messageService;

    @Autowired
    private CPALookupService cpaLookupService;

    @Autowired
    private JMSHeaderValidator headerValidator;

    @Autowired
    private MessageFactory messageFactory;

    @Override
    @Retryable(
            value = ClientTransportException.class, // infinite retry when ebMS Core endpoint is unavailable
            maxAttempts = Integer.MAX_VALUE,
            backoff = @Backoff(delay = 1000, maxDelay = 30000),
            listeners = "jmsRetryListener"
    )
    @Transactional
    public void onMessage(Message message) {

        try {
            if (!(message instanceof TextMessage)) {
                throw new IllegalArgumentException("JMS Message not supported: " + message.getClass());
            }

            headerValidator.validate(message);

            CPASearchCriteria searchCriteria = buildSearchCriteria((TextMessage) message);

            CollaborationProtocolAgreement cpa = cpaLookupService.findCPA(searchCriteria);
            String service = cpaLookupService.getService(cpa, searchCriteria);

            MessageRequest requestMessage = messageFactory.createMessage((TextMessage) message, cpa, service);

            MDC.setContextMap(LoggingUtils.getPropertyMap(requestMessage.getProperties()));

            LOGGER.info("Sending message");
            messageService.sendMessage(requestMessage);

        } catch (JMSException jmse) {
            throw new JMSHeaderValidationException(jmse);
        } catch (EbMSMessageServiceException_Exception e) {
            if (isDuplicateKeyException(e)) { // EbMS Core throws a DuplicateKeyException if the messageId already exists
                LOGGER.warn("Message was already processed. This could be due to a JMS redelivery");
            } else {
                throw new JmsConsumerException(e);
            }

        } finally {
            MDC.clear();
        }
    }

    private boolean isDuplicateKeyException(EbMSMessageServiceException_Exception e) {
        String message = e.getMessage();
        return message.contains("org.springframework.dao.DuplicateKeyException") && message.contains("ebms_message_pkey");
    }

    private CPASearchCriteria buildSearchCriteria(TextMessage message) throws JMSException {
        String sender = message.getStringProperty("x_aux_sender_id");
        String receiver = message.getStringProperty("x_aux_receiver_id");
        String action = message.getStringProperty("x_aux_action");
        String service = message.getStringProperty("x_aux_process_type");
        String serviceExtension = "$" + message.getStringProperty("x_aux_process_version");

        return new CPASearchCriteria(sender, receiver, action, service, serviceExtension);
    }

}
