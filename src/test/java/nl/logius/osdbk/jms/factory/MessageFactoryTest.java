package nl.logius.osdbk.jms.factory;

import nl.logius.osdbk.jms.cpa.jaxb.CPAParser;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.assertj.core.util.Files;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.util.ResourceUtils;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import org.oasis.open.ebxml.cpa.CollaborationProtocolAgreement;

class MessageFactoryTest {

    @Mock
    private MessageFactory messageFactory;

    private Session session;
    private ConnectionFactory connectionFactory;
    private CollaborationProtocolAgreement validCpa;


    @BeforeEach
    public void setup() throws IOException, JAXBException, JMSException {
        connectionFactory = new ActiveMQConnectionFactory("vm://localhost?broker.persistent=false");
        session = connectionFactory.createConnection().createSession(true, 0);

        File cpaFile = ResourceUtils.getFile("classpath:valid_cpa.xml");
        String cpaString = Files.contentOf(cpaFile, StandardCharsets.UTF_8);

        validCpa = parseCpa(cpaString);
        messageFactory = new MessageFactory();
    }

    @Test
    public void createEbMSMessageContentMessage() throws JMSException {
        TextMessage message = createTextMessage();
        String service = messageFactory.createMessage(message, validCpa, "dgl:ontvangen:1.0").getProperties().getService();
        Assertions.assertEquals("urn:osb:services:dgl:ontvangen:1.0", service);
    }

    private CollaborationProtocolAgreement parseCpa(String cpaString) throws JAXBException {
        return CPAParser.parse(cpaString);
    }

    private TextMessage createTextMessage() throws JMSException {
        TextMessage msg = session.createTextMessage("test1234");
        msg.setJMSMessageID("fakeMessageId");
        msg.setStringProperty("x_aux_system_msg_id", "1234");
        msg.setStringProperty("x_aux_action", "verstrekkingAanAfnemer");
        msg.setStringProperty("x_aux_process_instance_id", "conv-id");
        msg.setStringProperty("x_aux_process_type", "dgl:ontvangen:1.0");
        msg.setStringProperty("x_aux_process_version", "1.0");
        msg.setStringProperty("x_aux_sender_id", "00000004003214345001");
        msg.setStringProperty("x_aux_receiver_id", "22222222222222222222");
        msg.setStringProperty("x_aux_activity", "dgl:ontvangen:1.0");
        msg.setStringProperty("x_aux_production", "Production");
        msg.setStringProperty("x_aux_protocol", "ebMS");
        msg.setStringProperty("x_aux_protocol_version", "1.0");
        msg.setStringProperty("x_aux_process_instance_id", "a682c271-ba49-4644-a547-bf07b4692ce7");
        msg.setStringProperty("x_aux_seq_number", "0");
        msg.setStringProperty("x_aux_msg_order", "TRUE");
        return msg;
    }

}