package nl.logius.osdbk.jms.cpa;

import nl.logius.osdbk.jms.cpa.jaxb.CPAParser;
import org.assertj.core.util.Files;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.ResourceUtils;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import org.oasis.open.ebxml.cpa.CollaborationProtocolAgreement;

@ExtendWith(MockitoExtension.class)
class CPALookupServiceTest {

    @Mock
    private CachedCPAService cpaService;
    @InjectMocks
    private CPALookupService lookupService;

    private CollaborationProtocolAgreement validCPA;

    @BeforeEach
    public void setup() throws IOException, JAXBException {
        File cpaFile = ResourceUtils.getFile("classpath:valid_cpa.xml");
        String cpaString = Files.contentOf(cpaFile, StandardCharsets.UTF_8);
        validCPA = parseCpa(cpaString);
    }

    @Test
    void findCPA() throws JAXBException {
        when(cpaService.getCPAs()).thenReturn(Arrays.asList(validCPA));

        CPASearchCriteria searchCriteria = new CPASearchCriteria(
                "00000004003214345001",
                "22222222222222222222",
                "verstrekkingAanAfnemer",
                "dgl:ontvangen:1.0",
                "$1.0");

        CollaborationProtocolAgreement cpa = lookupService.findCPA(searchCriteria);

        assertEquals(validCPA.getCpaid(), cpa.getCpaid());
        assertEquals(validCPA.getPartyInfo().get(0).getPartyName(), cpa.getPartyInfo().get(0).getPartyName());
    }

    @Test
    void findCPANotFound() {
        when(cpaService.getCPAs()).thenReturn(new ArrayList<>());

        CPASearchCriteria searchCriteria = new CPASearchCriteria(
                "00000004003214345001",
                "22222222222222222222",
                "verstrekkingAanAfnemer",
                "dgl:ontvangen:1.0",
                "$1.0");

        Assertions.assertThrows(CPASelectionException.class, () -> {
            lookupService.findCPA(searchCriteria);
        });
    }

    @Test
    void findCPAMultipleResults() {
        when(cpaService.getCPAs()).thenReturn(Arrays.asList(validCPA, validCPA));

        CPASearchCriteria searchCriteria = new CPASearchCriteria(
                "00000004003214345001",
                "22222222222222222222",
                "verstrekkingAanAfnemer",
                "dgl:ontvangen:1.0",
                "$1.0");

        Assertions.assertThrows(CPASelectionException.class, () -> {
            lookupService.findCPA(searchCriteria);
        });
    }

    @Test
    void getService() throws JAXBException {
        CPASearchCriteria searchCriteria = new CPASearchCriteria(
                "00000004003214345001",
                "22222222222222222222",
                "verstrekkingAanAfnemer",
                "dgl:ontvangen:1.0",
                "$1.0");

        String service = lookupService.getService(validCPA, searchCriteria);
        assertEquals("dgl:ontvangen:1.0$1.0", service);
    }

    @Test
    void getServiceNotFoud() throws JAXBException {
        CPASearchCriteria searchCriteria = new CPASearchCriteria(
                "00000004003214345001",
                "22222222222222222222",
                "verstrekkingAanAfnemer",
                "dgl:verwerken:1.0",
                "$1.0");

        Assertions.assertThrows(CPASelectionException.class, () -> {
            lookupService.getService(validCPA, searchCriteria);
        });
    }

    private CollaborationProtocolAgreement parseCpa(String cpaString) throws JAXBException {
        return CPAParser.parse(cpaString);
    }

}