package nl.logius.osdbk.jms.cpa;

import nl.clockwork.cpa.CPAService;
import nl.clockwork.cpa.CPAServiceException_Exception;
import nl.logius.osdbk.jms.cpa.jaxb.CPAParser;
import org.assertj.core.util.Files;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.ResourceUtils;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import org.oasis.open.ebxml.cpa.CollaborationProtocolAgreement;

@ExtendWith(MockitoExtension.class)
class CachedCPAServiceTest {

    @Mock
    private CPAService cpaService;

    @InjectMocks
    private CachedCPAService cachedCPAService;

    private CollaborationProtocolAgreement validCpa;
    private String cpaString;

    @BeforeEach
    public void setup() throws JAXBException, FileNotFoundException {
        File cpaFile = ResourceUtils.getFile("classpath:valid_cpa.xml");
        cpaString = Files.contentOf(cpaFile, StandardCharsets.UTF_8);
        validCpa = CPAParser.parse(cpaString);
    }

    @Test
    void loadCPAs() throws CPAServiceException_Exception {
        when(cpaService.getCPAIds()).thenReturn(Arrays.asList(validCpa.getCpaid()));
        when(cpaService.getCPA(validCpa.getCpaid())).thenReturn(cpaString);
        cachedCPAService.loadCPAs();

        CollaborationProtocolAgreement cpa = cachedCPAService.getCPAs().get(0);

        assertEquals(validCpa.getCpaid(), cpa.getCpaid());
        assertEquals(validCpa.getPartyInfo().get(0).getPartyName(), cpa.getPartyInfo().get(0).getPartyName());
    }

    @Test
    void loadCPAsIgnoreInvalid() throws CPAServiceException_Exception {
        when(cpaService.getCPAIds()).thenReturn(Arrays.asList(validCpa.getCpaid(), "cpa-2"));
        when(cpaService.getCPA(validCpa.getCpaid())).thenReturn(cpaString);
        when(cpaService.getCPA("cpa-2")).thenReturn("<invalid-cpa>i-am-invalid</invalid-cpa>");
        cachedCPAService.loadCPAs();

        assertEquals(1, cachedCPAService.getCPAs().size()); // ignored invalid CPA
    }
}