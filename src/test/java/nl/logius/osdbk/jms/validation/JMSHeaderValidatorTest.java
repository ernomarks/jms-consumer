package nl.logius.osdbk.jms.validation;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.jms.*;

@ExtendWith(MockitoExtension.class)
class JMSHeaderValidatorTest {

    @InjectMocks
    private JMSHeaderValidator headerValidator;

    private Session session;
    private ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("vm://localhost?broker.persistent=false");

    @BeforeEach
    public void setup() throws JMSException {
        session = connectionFactory.createConnection().createSession(true, 0);
    }

    @Test
    void validate() throws JMSException {
        headerValidator.validate(createTextMessage());
    }

    @Test
    void validateInvalidHeader() throws JMSException {
        TextMessage message = createTextMessage();
        message.setStringProperty("x_aux_sender_id", "0000000400321434500112345"); // OIN of 25 chars

        Assertions.assertThrows(JMSHeaderValidationException.class, () -> {
            headerValidator.validate(message);
        });
    }

    private TextMessage createTextMessage() throws JMSException {
        TextMessage msg = session.createTextMessage("test1234");
        msg.setJMSMessageID("fakeMessageId");
        msg.setStringProperty("x_aux_system_msg_id", "1234");
        msg.setStringProperty("x_aux_action", "verstrekkingAanAfnemer");
        msg.setStringProperty("x_aux_process_instance_id", "conv-id");
        msg.setStringProperty("x_aux_process_type", "dgl:ontvangen:1.0");
        msg.setStringProperty("x_aux_process_version", "1.0");
        msg.setStringProperty("x_aux_sender_id", "00000004003214345001");
        msg.setStringProperty("x_aux_receiver_id", "22222222222222222222");
        msg.setStringProperty("x_aux_activity", "dgl:ontvangen:1.0");
        msg.setStringProperty("x_aux_production", "Production");
        msg.setStringProperty("x_aux_protocol", "ebMS");
        msg.setStringProperty("x_aux_protocol_version", "1.0");
        msg.setStringProperty("x_aux_process_instance_id", "a682c271-ba49-4644-a547-bf07b4692ce7");
        msg.setStringProperty("x_aux_seq_number", "0");
        msg.setStringProperty("x_aux_msg_order", "TRUE");

        return msg;
    }

}