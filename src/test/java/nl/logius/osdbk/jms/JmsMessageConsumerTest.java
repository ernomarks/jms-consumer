package nl.logius.osdbk.jms;

import nl.clockwork.ebms.*;
import nl.logius.osdbk.jms.cpa.CPALookupService;
import nl.logius.osdbk.jms.cpa.CPASearchCriteria;
import nl.logius.osdbk.jms.exception.JmsConsumerException;
import nl.logius.osdbk.jms.factory.MessageFactory;
import nl.logius.osdbk.jms.validation.JMSHeaderValidationException;
import nl.logius.osdbk.jms.validation.JMSHeaderValidator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import javax.jms.JMSException;
import javax.jms.TextMessage;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import org.oasis.open.ebxml.cpa.CollaborationProtocolAgreement;

@ExtendWith(MockitoExtension.class)
class JmsMessageConsumerTest {

    @Mock
    private EbMSMessageService messageService;

    @Mock
    private CPALookupService cpaLookupService;

    @Mock
    private JMSHeaderValidator headerValidator;

    @Mock
    private MessageFactory messageFactory;

    @InjectMocks
    private JmsMessageConsumer messageConsumer;

    @Mock
    private TextMessage message;

    @Mock
    private MessageRequest content;

    @Mock
    private CollaborationProtocolAgreement cpa;

    @Mock
    private MessageRequestProperties context;
    
    @Test
    void onMessage() throws JMSException, EbMSMessageServiceException_Exception {
        when(context.getFromPartyId()).thenReturn("fromParty");
        when(context.getFromRole()).thenReturn("fromRole");
        when(context.getToPartyId()).thenReturn("toParty");
        when(context.getToRole()).thenReturn("toRole");

        when(cpaLookupService.findCPA(any(CPASearchCriteria.class))).thenReturn(cpa);
        when(cpaLookupService.getService(eq(cpa), any(CPASearchCriteria.class))).thenReturn("service");
        when(messageFactory.createMessage(message, cpa, "service")).thenReturn(content);
        when(content.getProperties()).thenReturn(context);

        messageConsumer.onMessage(message);
        verify(messageService, times(1)).sendMessage(content);
    }

    @Test
    void onMessageJMSExceptionInValidation() throws JMSException {
        doThrow(new JMSException("oops.")).when(headerValidator).validate(message);

        Assertions.assertThrows(JMSHeaderValidationException.class, () -> {
            messageConsumer.onMessage(message);
        });
    }

    @Test
    void onMessageDuplicateException() throws JMSException, EbMSMessageServiceException_Exception {
        String duplicateErrorMessage = "org.springframework.dao.DuplicateKeyException: PreparedStatementCallback; ERROR: duplicate key value violates unique constraint \"ebms_message_pkey\"";

        when(context.getFromPartyId()).thenReturn("fromParty");
        when(context.getFromRole()).thenReturn("fromRole");
        when(context.getToPartyId()).thenReturn("toParty");
        when(context.getToRole()).thenReturn("toRole");


        when(cpaLookupService.findCPA(any(CPASearchCriteria.class))).thenReturn(cpa);
        when(cpaLookupService.getService(eq(cpa), any(CPASearchCriteria.class))).thenReturn("service");
        when(messageFactory.createMessage(message, cpa, "service")).thenReturn(content);
        when(content.getProperties()).thenReturn(context);


        when(messageService.sendMessage(content)).thenThrow(new EbMSMessageServiceException_Exception(duplicateErrorMessage, mock(EbMSMessageServiceException.class)));

        messageConsumer.onMessage(message); // ignores duplicate
        verify(messageService, times(1)).sendMessage(content);
    }

    @Test
    void onMessageException() throws JMSException, EbMSMessageServiceException_Exception {
        String errorMessage = "oops.";

        when(context.getFromPartyId()).thenReturn("fromParty");
        when(context.getFromRole()).thenReturn("fromRole");
        when(context.getToPartyId()).thenReturn("toParty");
        when(context.getToRole()).thenReturn("toRole");


        when(cpaLookupService.findCPA(any(CPASearchCriteria.class))).thenReturn(cpa);
        when(cpaLookupService.getService(eq(cpa), any(CPASearchCriteria.class))).thenReturn("service");
        when(messageFactory.createMessage(message, cpa, "service")).thenReturn(content);
        when(content.getProperties()).thenReturn(context);

        when(messageService.sendMessage(content)).thenThrow(new EbMSMessageServiceException_Exception(errorMessage, mock(EbMSMessageServiceException.class)));

        Assertions.assertThrows(JmsConsumerException.class, () -> {
            messageConsumer.onMessage(message);
        });
    }

}